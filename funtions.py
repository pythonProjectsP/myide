from tkinter import *
from tkinter import ttk, messagebox, scrolledtext
import mysql.connector
from mysql.connector import errorcode
import platform
import easygui as msg

principalwindow = None 
contador = None 
secondwindow = None 
validarcombo = None 
validarhost = None
validarport = None 
validaruser = None 
validarpassword = None
labelname = None
labelhost = None
labelport = None
labeluser = None
labelpassword = None
labeldatabase = None
labelexcelente = None 
labelerror = None 
combo = None 
camponame = None
campohost = None 
campoport = None 
campouser = None 
campopassword = None 
campodatabase = None 
secondwindow = None 
port = None
pestana = None
connect = None
scroll = None

def init():
    global principalwindow
    system = platform.system()
    if system == 'Linux':
        principalwindow = Tk()
        principalwindow.title('My Ide')
        principalwindow.attributes('-zoomed', True)

        # Menu
        menubar = Menu(principalwindow)

        # Sub Menus
        archivo = Menu(menubar, tearoff=0)
        editar = Menu(menubar, tearoff=0)

        # Submenu para archivo
        archivo.add_command(label='Nuevo', command=second, font=('Arial', 13))
        archivo.add_command(label='Abrir', font=('Arial', 13))
        archivo.add_command(label='Guardar', font=('Arial', 13))
        archivo.add_command(label='Guardar como', font=('Arial', 13))
        archivo.add_separator()
        archivo.add_command(label='Cerrar', command=principalwindow.quit, font=('Arial', 13))

        # Submenu para editar
        editar.add_command(label='Copiar')

        #
        menubar.add_cascade(label='Archivo', font=('Arial', 14),menu=archivo)

        principalwindow.config(background='#7E7C7C', menu=menubar)

        principalwindow.mainloop()
    elif system == 'Windows':
        principalwindow = Tk()
        principalwindow.title('My Ide')
        principalwindow.attributes('-fullscreen', True)

        # Menu
        menubar = Menu(principalwindow)

        # Sub Menus
        archivo = Menu(menubar, tearoff=0)
        editar = Menu(menubar, tearoff=0)

        # Submenu para archivo
        archivo.add_command(label='Nuevo', command=second, font=('Arial', 13))
        archivo.add_command(label='Abrir', font=('Arial', 13))
        archivo.add_command(label='Guardar', font=('Arial', 13))
        archivo.add_command(label='Guardar como', font=('Arial', 13))
        archivo.add_separator()
        archivo.add_command(label='Cerrar', command=principalwindow.quit, font=('Arial', 13))

        # Submenu para editar
        editar.add_command(label='Copiar')

        #
        menubar.add_cascade(label='Archivo', font=('Arial', 14),menu=archivo)

        principalwindow.config(background='#7E7C7C', menu=menubar)

        principalwindow.mainloop()

# Function to center window
def center(toplevel):
    toplevel.update_idletasks()
    w = toplevel.winfo_screenwidth()
    h = toplevel.winfo_screenheight()
    size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
    x = w/2 - size[0]/2
    y = h/2 - size[1]/2 
    toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))

def data():
    datos = scroll.get('1.0', END)
    dataa = datos.replace('\n', ' ').split(';')
    print(dataa)

# funtion to connect
def conectar():
    
    global connect, scroll
    # Var
    combob = combo.get()
    host = campohost.get()
    port = campoport.get()
    user = campouser.get()
    password = campopassword.get()
    database = campodatabase.get()

    #note.place(x=0, y=0)
    #note.pressed_index = None
    namepes = camponame.get()
    nombre = []
    nombre.append(namepes)
    lista_de_nombres = []
    pesta = 'pestana'
    miestilo = 'mi_estilo'
    style = ttk.Style()

    if combob == 'Seleccione una Opción':
        # validarcombo = Label(secondwindow, text='Debe seleccionar una de las opciones', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
        # validarcombo.place(x=10, y=37)
        msg.msgbox(msg='Debes seleccionar una de las opciones', title='Error', ok_button='Ok')
    elif combob == 'MySQL':
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            # validarcombo.place_forget()
            # campohost.insert(0, 'localhost')
            msg.msgbox(msg='El campo host está vacío.', title='Error', ok_button='Ok')
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            # campoport.insert(0, '3306')
            msg.msgbox(msg='El campo host está vacío.', title='Error', ok_button='Ok')
        if user == '':
            # validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validaruser.place(x=10, y=210)
            msg.msgbox(msg='El campo host está vacío.', title='Error', ok_button='Ok')
        if password == '':
            # validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarpassword.place(x=260, y=210)
            msg.msgbox(msg='El campo host está vacío.', title='Error', ok_button='Ok')
        else:
            #validaruser.place_forget()
            #validarpassword.place_forget()
            #connectwithdatabase = mysql.connector.connect(host=host, port=port, passwd=password, database=database, auth_plugin='mysql_native_password')
            if database == '':
                try:
                    connect = mysql.connector.connect(host=host, port=port, user=user, passwd=password, auth_plugin='mysql_native_password')
                    #messagebox.showinfo(title='Bien hecho', message='Excelente')
                    labelexcelente = Label(secondwindow, text='Su conexion fue satisfactoria.', font=('Arial', 16), bg='#133d01', fg='#FFFFFF')
                    labelexcelente.place(x=80, y=305)
                    #labelerror.place_forget()

                    # Crear pestanas
                    note = ttk.Notebook(principalwindow)
                    note.pack(fill='both', expand='yes')

                    config = {'TNotebook.Tab': {'configure': {'padding': [5, 1], 'background': '#D1CCC3'}, 
                                                'map': {'background': [('selected', '#626E93'), ('active', '#435D5D')],
                                                'foreground': [('selected', '#ffffff'), ('active', '#000000')]}}}

                    contador = 0
                    for i in range(len(nombre)):
                        #
                        mystyle = nombre[contador] + str(i)
                        #
                        style.theme_create(mystyle, parent='alt', settings=config)
                        style.theme_use(mystyle)
                        pestana = pesta + str(i+2)
                        #
                        # pestana = nombre[contador] + str(i)
                        # pestana += nombre[contador]
                        #
                        pestana = ttk.Frame(note)
                        # #
                        name = nombre[i] + '.sql'
                        # #
                        note.add(pestana, text=name)
                        # #
                        sroll = Scrollbar(note)
                        scroll = scrolledtext.ScrolledText(pestana, width=100, height=34, wrap=WORD, yscrollcommand=sroll.set)
                        scroll.place(x=0, y=0)
                        lista_de_nombres = nombre[0]
                        nombre.pop(contador)
                        boton = Button(pestana, text='Run', font=('Arial', 12), command=data)
                        boton.place(x=700, y=100)

                        destruir()
                except mysql.connector.Error as err:
                    #messagebox.showerror(title='Ocurrio un error', message=err.msg)
                    labelexcelente.place_forget()
                    labelerror = Label(secondwindow, text=err.msg, font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
                    labelerror.place(x=10, y=305)
                    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                        messagebox.showerror(title='Error', message=err.msg)
                    elif err.errno == errorcode.ER_BAD_DB_ERROR:
                        messagebox.showerror(title='Error', message='La Base de datos no existe')
                    else:
                        messagebox.showerror(title='Error', message=err)
                else:
                    connect.close()
            else:
                pass

    elif combob == 'Oracle':
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            campohost.insert(0, 'localhost')
            validarcombo.place_forget()
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            campoport.insert(0, '3306')
        if user == '':
            validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validaruser.place(x=10, y=210)
        if password == '':
            validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validarpassword.place(x=260, y=210)
    elif combob == 'MongoDB':
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            campohost.insert(0,'localhost')
            validarcombo.place_forget()
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            campoport.insert(0, '3306')
        if user == '':
            validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validaruser.place(x=10, y=210)
        if password == '':
            validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validarpassword.place(x=260, y=210)

    elif combob == 'PostgreSQL':
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            campohost.insert(0, 'localhost')
            validarcombo.place_forget()
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            campoport.insert(0, '3306')
        if user == '':
            validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validaruser.place(x=10, y=210)
        if password == '':
            validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validarpassword.place(x=260, y=210)

        # for elemente in nombre:
        #     elemente = ttk.Frame(note)
        #     name = nombre[i] + '.sql'
        #     note.add(elemente, text=name)
        #     print(elemente)

        #pestana = ttk.Frame(note)
        #pestana2 = ttk.Frame(note)
        #note.add(pestana, text="Probar")
        #print(pestana)

    # Creamos el scroll
    #sroll = Scrollbar(note)
    #scroll = scrolledtext.ScrolledText(pestana1, width=100, height=33, yscrollcommand=sroll.set)
    #scroll.place(x=0, y=0)

#
def destruir():
    secondwindow.destroy()

# Function to test the connection
def test():
    # Var
    global validarcombo, validarhost, validarport, validaruser, validarpassword, labelexcelente, labelerror

    # Var
    combob = combo.get()
    name = camponame.get()
    host = campohost.get()
    port = campoport.get()
    user = campouser.get()
    password = campopassword.get()
    database = campodatabase.get()

    if combob == 'Seleccione una Opción':
        # validarcombo = Label(secondwindow, text='Debe seleccionar una de las opciones', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
        # validarcombo.place(x=10, y=39)
        msg.msgbox(msg='Debes seleccionar una de las opciones', title='Error', ok_button='Ok')
    elif combob == 'MySQL':
        if name == '':
            # validarname = Label(secondwindow, text='El campo name está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarname.place(x=10, y=115)
            msg.msgbox(msg='El campo name está vacío.', title='Error', ok_button='Ok')
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            # validarcombo.place_forget()
            # campohost.insert(0, 'localhost')
            msg.msgbox(msg='El campo host está vacío.', title='Error', ok_button='Ok')
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            # campoport.insert(0, '3306')
            msg.msgbox(msg='El campo port está vacío.', title='Error', ok_button='Ok')
        if user == '':
            # validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validaruser.place(x=10, y=297)
            msg.msgbox(msg='El campo user está vacío.', title='Error', ok_button='Ok')
        if password == '':
            # validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarpassword.place(x=310, y=297)
            msg.msgbox(msg='El campo password está vacío.', title='Error', ok_button='Ok')
        else:
            # validaruser.place_forget()
            # validarpassword.place_forget()
            #connectwithdatabase = mysql.connector.connect(host=host, port=port, passwd=password, database=database, auth_plugin='mysql_native_password')
            if database == '':
                try:
                    connect = mysql.connector.connect(host=host, port=port, user=user, passwd=password, auth_plugin='mysql_native_password')
                    #messagebox.showinfo(title='Bien hecho', message='Excelente')
                    labelexcelente = Label(secondwindow, text='Su conexion fue satisfactoria.', font=('Arial', 16), bg='#133d01', fg='#FFFFFF')
                    labelexcelente.place(x=120, y=400)
                    #labelerror.place_forget()
                except mysql.connector.Error as err:
                    #messagebox.showerror(title='Ocurrio un error', message=err.msg)
                    labelexcelente.place_forget()
                    labelerror = Label(secondwindow, text=err.msg, font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
                    labelerror.place(x=10, y=305)
            else:
                pass

    elif combob == 'Oracle':
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            campohost.insert(0, 'localhost')
            validarcombo.place_forget()
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            campoport.insert(0, '3306')
        if user == '':
            validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validaruser.place(x=10, y=210)
        if password == '':
            validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validarpassword.place(x=260, y=210)
    elif combob == 'MongoDB':
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            campohost.insert(0,'localhost')
            validarcombo.place_forget()
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            campoport.insert(0, '3306')
        if user == '':
            validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validaruser.place(x=10, y=210)
        if password == '':
            validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validarpassword.place(x=260, y=210)

    elif combob == 'PostgreSQL':
        if host == '':
            # validarhost = Label(secondwindow, text='El campo host está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarhost.place(x=10, y=120)
            campohost.insert(0, 'localhost')
            validarcombo.place_forget()
        if port == '':
            # validarport = Label(secondwindow, text='El campo port está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            # validarport.place(x=260, y=120)
            # validarhost.place_forget()
            campoport.insert(0, '3306')
        if user == '':
            validaruser = Label(secondwindow, text='El campo user está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validaruser.place(x=10, y=210)
        if password == '':
            validarpassword = Label(secondwindow, text='El campo password está vacío.', font=('Arial', 12), bg='#FE0202', fg='#FFFFFF')
            validarpassword.place(x=260, y=210)


# Funtion for second windows
def second():
    # Var
    global combo, camponame, campohost, campoport, campouser, campopassword, campodatabase, secondwindow, port
    global labelname, labelhost, labelport, labeluser, labelpassword, labeldatabase, labelexcelente, labelerror 

    port = IntVar()

    secondwindow = Tk()
    secondwindow.title('')
    secondwindow.geometry('600x500+70+60')

    # Combo
    combo = ttk.Combobox(secondwindow, font=('Arial', 14), state='readonly', width=56)
    combo.set('Seleccione una Opción')
    combo['values'] = ['MySQL', 'Oracle', 'MongoDB', 'PostgreSQL']
    combo.place(x=10, y=10)

    # Label´s
    labelname = Label(secondwindow, font=('Arial', 12), text='Name', bg='#2F2C2C', fg='#FEFCFC')
    labelname.place(x=10, y=65)
    labelhost = Label(secondwindow, font=('Arial', 12), text='Host', bg='#2F2C2C', fg='#FEFCFC')
    labelhost.place(x=10, y=155)
    labelport = Label(secondwindow, font=('Arial', 12), text='Port', bg='#2F2C2C', fg='#FEFCFC')
    labelport.place(x=310, y=155)
    labeluser = Label(secondwindow, font=('Arial', 12), text='User', bg='#2F2C2C', fg='#FEFCFC')
    labeluser.place(x=10, y=245)
    labelpassword = Label(secondwindow, font=('Arial', 12), text='Password', bg='#2F2C2C', fg='#FEFCFC')
    labelpassword.place(x=310, y=245)
    labeldatabase = Label(secondwindow, font=('Arial', 12), text='Database', bg='#2F2C2C', fg='#FEFCFC')
    labeldatabase.place(x=10, y=335)

    # Entry´s
    camponame = Entry(secondwindow, font=('Arial', 12), width='63', bg='#CCCACA', fg='#000000')
    camponame.place(x=10, y=88)
    campohost = Entry(secondwindow, font=('Arial', 12), width='30', bg='#CCCACA', fg='#000000')
    campohost.place(x=10, y=178)
    campoport = Entry(secondwindow, font=('Arial', 12), width='30', bg='#CCCACA', fg='#000000')
    campoport.place(x=310, y=178)
    campouser = Entry(secondwindow, font=('Arial', 12), width='30', bg='#CCCACA', fg='#000000')
    campouser.place(x=10, y=268)
    campopassword = Entry(secondwindow, font=('Arial', 12), width='30', show='*', bg='#CCCACA', fg='#000000')
    campopassword.place(x=310, y=268)
    campodatabase = Entry(secondwindow, font=('Arial', 12), width='63', bg='#CCCACA', fg='#000000')
    campodatabase.place(x=10, y=358)

    # Botones
    botontest = Button(secondwindow, text='Test', font=('Arial', 12), bg='#BFA500', command=test)
    botontest.place(x=440, y=465)
    botonConexion = Button(secondwindow, text='Connect', font=('Arial', 12), bg='#154D04', fg='#FFFFFF', command=conectar)
    botonConexion.place(x=505, y=465)

    secondwindow.config(bg='#2F2C2C')
    center(secondwindow)
    secondwindow.mainloop()
